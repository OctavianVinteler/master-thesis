'use strict';

var BpmnJS = window.BpmnJS,
    $ = window.jQuery;

var viewer = new BpmnJS({ container: '#diagram' });

// Global constants for element status
var EXECUTED_STATUS = 'executed';
var SELECTABLE_STATUS = 'selectable';
var EXECUTABLE_STATUS = 'executable';

// Global arrays
var activeElements = [];
var successorsLists = {};
var predecessorsLists = {};
var selectableElements = [];

// Business Collections
var elementOverlays = {};
var elementConditions = {};

// Global Json-editor data
var scriptEditor;
var conditionEditor;
var overlayEditor;
var activationEditor;
var taskEditor;
var startEventEditor;

// Global Json-editor windows
var scriptWindow;
var conditionWindow;
var overlayWindow;
var activationWindow;
var taskWindow;
var startEventWindow;

// Global Data Object
var businessData;
var formProperties;

// Data for Determining OR Gates Enablement
var orJoins = [];
var redHash = {};
var greenHash = {};
var tokenElements = [];

// Arrays for other types of join gateways
var andJoins = [];
var xorJoins = [];

// Buttons
var stepButton;
var beginButton;

// Global Variables
var simulationBegan;

//------------------------------- Util Functions--------------------------------
function setIntersection(a, b) {
  var t;
  if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
  return a.filter(function (e) {
      if (b.indexOf(e) !== -1) return true;
  });
}

//------------------------------- Json Editor-----------------------------------
function initializeJsonEditorData() {
  scriptEditor = new JSONEditor(document.getElementById('scriptForm'), {
    template: 'hogan',
    schema: {
      type: "object",
      options: {
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true
      },
      title: "Script",
      properties: {
        script: {
          type: "string"
        }
      }
    }
  });

  scriptWindow = $("#scriptWindow").dialog({
    modal: true,
    autoOpen: false,
    minWidth: 600,
    minHeight: 220
  });

  scriptWindow.dialog('close');

  conditionEditor = new JSONEditor(document.getElementById('conditionForm'), {
    template: 'hogan',
    schema: {
      type: "object",
      options: {
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true
      },
      title: "Condition",
      properties: {
        condition: {
          type: "string"
        }
      }
    }
  });

  conditionWindow = $("#conditionWindow").dialog({
    modal: true,
    autoOpen: false,
    minWidth: 600,
    minHeight: 220
  });

  conditionWindow.dialog('close');

  overlayEditor = new JSONEditor(document.getElementById('overlayForm'), {
    template: 'hogan',
    schema: {
      type: "object",
      options: {
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true
      },
      title: "Overlay",
      properties: {
        overlay: {
          type: "string"
        }
      }
    }
  });

  overlayWindow = $("#overlayWindow").dialog({
    modal: true,
    autoOpen: false,
    minWidth: 600,
    minHeight: 220
  });

  overlayWindow.dialog('close');

  activationEditor = new JSONEditor(document.getElementById('activationForm'), {
    template: 'hogan',
    schema: {
      type: "object",
      options: {
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true
      },
      title: "Activation",
      properties: {
        Activate: {
          type: "boolean",
          format: "checkbox"
        }
      }
    }
  });

  activationWindow = $("#activationWindow").dialog({
    modal: true,
    autoOpen: false
  });

  activationWindow.dialog('close');

  taskEditor = new JSONEditor(document.getElementById('taskForm'), {
    theme: 'bootstrap2',
    schema: {
      type: "object",
      options: {
        disable_collapse: true,
        disable_edit_json: true,
        disable_properties: true
      },
      title: "Task",
      properties: {
      }
    }
  });

  taskWindow = $("#taskWindow").dialog({
    modal: true,
    autoOpen: false,
    minWidth: 500
  });

  taskWindow.dialog('close');

  startEventEditor = new JSONEditor(document.getElementById('startEventForm'), {
    template: 'hogan',
    schema: {
      type: "array",
      format: "table",
      uniqueItems: true,
      items: {
        type: "object",
        options: {
          disable_collapse: true,
          disable_edit_json: true,
          disable_properties: true
        },
        properties: {
          name: {
            type: "string"
          },
          type: {
            type: "string",
            enum: ["string", "date", "integer", "boolean"]
          },
          default: {
            type: "string"
          }
        }
      }
    }
  });

  startEventWindow = $("#startEventWindow").dialog({
    modal: true,
    autoOpen: false,
    minWidth: 600
  });

  startEventWindow.dialog('close');
}

function newScriptWindow(element, moodle) {
  if(element.businessObject.script == null) {
    scriptEditor.getEditor('root.script').setValue("");
  }
  else {
    scriptEditor.getEditor('root.script')
                    .setValue(element.businessObject.script.body);
  }

  scriptWindow.dialog('open');

  // Clone the button in order to get rid of the event listeners
  var oldButton = document.getElementById('submitScript')
  var buttonClone = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(buttonClone, oldButton);

  document.getElementById('submitScript').addEventListener('click', function() {
    //TODO - how to determine the existance of the variables here
    element.businessObject.script = moodle.create('bpmn:FormalExpression', {
                                      body: scriptEditor.getValue().script,
                                      name: element.id
                                    });
    updateConditionVariables();
    scriptWindow.dialog('close');
  });
}

function newConditionWindow(element, moodle) {
  if(element.businessObject.conditionExpression == null) {
    conditionEditor.getEditor('root.condition').setValue("");
  }
  else {
    conditionEditor.getEditor('root.condition')
                    .setValue(element.businessObject.conditionExpression.body);
  }

  conditionWindow.dialog('open');

  // Clone the button in order to get rid of the event listeners
  var oldButton = document.getElementById('submitCondition')
  var buttonClone = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(buttonClone, oldButton);

  document.getElementById('submitCondition').addEventListener('click', function() {
    element.businessObject.conditionExpression =
                                    moodle.create('bpmn:FormalExpression', {
                                      body: conditionEditor.getValue().condition,
                                      name: element.id
                                    });
    conditionWindow.dialog('close');
  });
}

function newGateOverlayWindow(element, overlays, moodle) {
  if(element.businessObject.overlay == null) {
    overlayEditor.getEditor('root.overlay').setValue("");
  }
  else {
    overlayEditor.getEditor('root.overlay')
                    .setValue(element.businessObject.overlay.body);
  }

  overlayWindow.dialog('open');

  // Clone the button in order to get rid of the event listeners
  var oldButton = document.getElementById('submitOverlay')
  var buttonClone = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(buttonClone, oldButton);

  document.getElementById('submitOverlay').addEventListener('click', function() {
    overlays.remove(element);
    overlays.add(element, {
      position: {
        bottom: 0,
        right: 0
      },
      html: overlayEditor.getValue().overlay
    });
    element.businessObject.overlay = moodle.create('bpmn:FormalExpression', {
                                      body: overlayEditor.getValue().overlay,
                                      name: element.id
                                    });
    overlayWindow.dialog('close');
  });
}

function newActivationWindow(element, canvas, elementRegistry) {
  activationEditor.getEditor('root.Activate').setValue("");
  activationWindow.dialog('open');

  // Clone the button in order to get rid of the event listeners
  var oldButton = document.getElementById('submitActivation')
  var buttonClone = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(buttonClone, oldButton);

  document.getElementById('submitActivation').addEventListener('click', function() {
    var act = activationEditor.getEditor('root.Activate').getValue();
    if(act) {
      selectElement(element, canvas, elementRegistry);
    }
    else {
      deselectElement(element, canvas, elementRegistry);
    }
    activationEditor.getEditor('root.Activate').setValue("");
    activationWindow.dialog('close');
  });
}

function newTaskWindow(element) {
  taskWindow.dialog('open');

  // Clone the button in order to get rid of the event listeners
  var oldButton = document.getElementById('submitTask')
  var buttonClone = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(buttonClone, oldButton);

  document.getElementById('submitTask').addEventListener('click', function() {
    taskWindow.dialog('close');
  });
}

// Define the properties which are used in the simulation
function newStartEventWindow(element) {
  startEventWindow.dialog('open');

  // Clone the button in order to get rid of the event listeners
  var oldButton = document.getElementById('submitStartEvent')
  var buttonClone = oldButton.cloneNode(true);
  oldButton.parentNode.replaceChild(buttonClone, oldButton);

  document.getElementById('submitStartEvent').addEventListener('click', function() {
    formProperties.splice(0,formProperties.length)
    startEventWindow.dialog('close');
  });
}

function initilizeTaskWindowEditor() {
  var items = startEventEditor.getEditor('root').getValue();
  var propText = "{";

  items.forEach(function(entry) {
    propText += "\"" + entry.name + "\"" + ": {";
    if(entry.type == "string") {
      propText += "\"type\": \"string\"},"
    }
    else if(entry.type == "date") {
      propText += "\"type\": \"string\", \"format\": \"date\"},"
    }
    else if(entry.type == "integer") {
      propText += "\"type\": \"integer\"},"
    }
    else if(entry.type == "boolean") {
      propText += "\"type\": \"boolean\", \"format\": \"checkbox\"},"
    }
  });

  // Remove the last comma from the generated json
  if(propText.slice(-1) == ",") {
    propText = propText.substring(0, propText.length - 1);
  }
  propText += "}";
  var propObject = JSON.parse(propText);

  document.getElementById('taskForm').innerHTML = "";
  taskEditor = new JSONEditor(document.getElementById('taskForm'), {
    schema: {
      type: "object",
      title: "Task",
      properties: propObject
    }
  });

  // initialize with default values
  items.forEach(function(entry) {
    taskEditor.getEditor('root.' + entry.name).setValue(entry.default);
  });
}
//------------------------------------------------------------------------------

//------------------------------- BPMN Visual Elements Functions----------------
function removeMarkers(element, canvas) {
  canvas.removeMarker(element, 'active');
  canvas.removeMarker(element, 'highlight');
  canvas.removeMarker(element, 'selectable');
  canvas.removeMarker(element, 'waiting');
}

function activateElement(element, canvas) {
  element.status = EXECUTABLE_STATUS;
  removeMarkers(element, canvas);
  canvas.addMarker(element, 'active');
}

function highlightElement(element, canvas) {
  removeMarkers(element, canvas);
  canvas.addMarker(element, 'highlight');
  element.status = EXECUTED_STATUS;
}

function selectableElement(element, canvas) {
  element.status = SELECTABLE_STATUS;
  removeMarkers(element, canvas);
  canvas.addMarker(element, 'selectable');
}

function waitingElement(element, canvas) {
  element.status = 'waiting';
  removeMarkers(element, canvas);
  canvas.addMarker(element, 'waiting');
}

function updateConditionVariables() {
  var varList = document.getElementById("variablesList");
  varList.innerHTML = "";

  for(var key in businessData){
    var spanEl = document.createElement('h4');
    spanEl.innerHTML = key + ' = ' + businessData[key]
    varList.appendChild(spanEl);
  }
}
//------------------------------------------------------------------------------

//-------------------------------- BPMN Elements Type Tests---------------------
function isConnection(element) {
  return (element.type == 'bpmn:SequenceFlow' ||
          element.type == 'bpmn:MessageFlow');
}

function isExecutableElement(element) {
  return (element.type.indexOf('Event') > -1 ||
          element.type.indexOf('Task') > -1 ||
          element.type.indexOf('Gateway') > -1)
}

function isTask(element) {
  return element.type.indexOf('Task') > -1;
}

function isScriptTask(element) {
  return element.type == 'bpmn:ScriptTask';
}

function isXORGateway(element) {
  return element.type == 'bpmn:ExclusiveGateway';
}

function isXORSplitGateway(element) {
  return (element.type == 'bpmn:ExclusiveGateway'
            && element.outgoing.length > 1);
}

function isXORJoinGateway(element) {
  return (element.type == 'bpmn:ExclusiveGateway'
            && element.incoming.length > 1);
}

function isANDGateway(element) {
  return element.type == 'bpmn:ParallelGateway';
}

function isANDJoinGateway(element) {
  return element.type == 'bpmn:ParallelGateway' && element.incoming.length > 1;
}

function isORJoinGateway(element) {
  return (element.type == 'bpmn:InclusiveGateway'
            && element.incoming.length > 1);
}

function isORGateway(element) {
  return element.type == 'bpmn:InclusiveGateway';
}

function isORSplitGateway(element) {
  return (element.type == 'bpmn:InclusiveGateway'
            && element.outgoing.length > 1);
}

function isEventBasedGateway(element) {
  return element.type == 'bpmn:EventBasedGateway';
}

function isEventBasedSplitGateway(element) {
  return (element.type == 'bpmn:EventBasedGateway'
            && element.outgoing.length > 1);
}

function isGateway(element) {
  return element.type.indexOf('Gateway') > -1;
}

function isJoinGateway(element) {
  return element.type.indexOf('Gateway') > -1
            && element.incoming.length > 1;
}

function isStartEvent(element) {
  return element.type.indexOf('StartEvent') > -1;
}

function isEvent(element) {
  return element.type.indexOf('Event') > -1;
}
//------------------------------------------------------------------------------

//-------------------------------- Process Adjacency Lists----------------------
function getInitialActiveElements(elements) {
  var candidateIds = [];

  elements.forEach(function(entry) {
    if(isStartEvent(entry)) {
      candidateIds.push(entry.id);
    }
  });

  return candidateIds;
}

function getElementIds(elementRegistry) {
  var elementIds = [];

  elementRegistry.forEach(function(entry) {
    if(isExecutableElement(entry)) {
      elementIds.push(entry.id);
    }
  });

  return elementIds;
}

function getElementByName(elementRegistry, name) {
  var elementEntry = elementRegistry._elements[name];

  if(elementEntry != null) {
    return elementEntry.element;
  }

  return null;
}
//------------------------------------------------------------------------------

//------------------------------ Handle BPMN Elements---------------------------
function handleElement(element, canvas, elementRegistry, eliminateDuplicates) {
  if(isGateway(element)) {
    handleGate(element, elementRegistry, canvas, element.id);
  }
  else {
    setElementAsExecuted(element, canvas);
    updateTokens(element, elementRegistry);

    // if the task has an exception event attached
    if(element.attachers.length > 0) {
      var nextElements = [];

      // Get all elements which are successors of the current task
      successorsLists[element.id].forEach(function(entry) {
        nextElements.push(getElementByName(elementRegistry, entry));
      });

      // Get all elements that are attached to the current task
      element.attachers.forEach(function(entry) {
        nextElements.push(entry);
      });

      // mark all successors as selectable
      nextElements.forEach(function(entryElement) {
        selectableElement(entryElement, canvas);
        entryElement.selectableId = element.id;
        selectableElements.push(entryElement);
      });
    }
    // the task doesn't have any event attached
    else {
      activeElements = activeElements.concat(successorsLists[element.id]);
      if(eliminateDuplicates) {
        activeElements = activeElements.filter(function(item, pos) {
          return activeElements.indexOf(item) == pos;
        });
      }
    }
  }
}

function activateNextElements(elementRegistry, canvas) {

  activeElements.forEach(function(entry) {
    var element = getElementByName(elementRegistry, entry);

    if(isJoinGateway(element)) {
      // do nothing
    }
    else if(element.status != SELECTABLE_STATUS) {
      activateElement(element, canvas);
    }
  });

  activateJoinGateways(canvas);
}

function updateORHashes(elementRegistry) {
  var redPred;
  var greenPred;

  // Update red hash
  orJoins.forEach(function(orJ) {
    redPred = [];
    greenPred = [];
    redHash[orJ.id] = [];
    greenHash[orJ.id] = [];

    orJ.incoming.forEach(function(incomingFlow){
      if(tokenElements.indexOf(incomingFlow) >= 0){
        redPred.push(incomingFlow);
      }
      else {
        greenPred.push(incomingFlow);
      }
    });

    // For Red Hashmap
    while (redPred.length > 0) {
      var connection = redPred.pop();
      if(redHash[orJ.id].indexOf(connection) < 0) {
        redHash[orJ.id].push(connection);
        connection.incoming.forEach(function(predEl){
          if(predEl != orJ) {
            // for each connection incoming to this element
            predEl.incoming.forEach(function(con){
              if(redPred.indexOf(con) < 0 && redHash[orJ.id].indexOf(con) < 0) {
                redPred.push(con);
              }
            });
          }
        });
      }
    }

    // For Green Hashmap
    while (greenPred.length > 0) {
      var connection = greenPred.pop();
      if(greenHash[orJ.id].indexOf(connection)) {
        greenHash[orJ.id].push(connection);
        connection.incoming.forEach(function(predEl){
          if(predEl != orJ) {
            // for each connection incoming to this element
            predEl.incoming.forEach(function(con){
              if(greenPred.indexOf(con) < 0 && redPred.indexOf(con) < 0
                    && greenHash[orJ.id].indexOf(con) < 0) {
                greenPred.push(con);
              }
            });
          }
        });
      }
    }
  });
}

function activateJoinGateways(canvas) {

  // activate OR joins
  orJoins.forEach(function(orJ) {
    if(orJ.status == SELECTABLE_STATUS) {
      // do nothing because the gate is subject to a selection process
    }
    else {
      var activationIntersection = true;
      var oneIncomingActive = false;

      if(setIntersection(orJ.incoming, tokenElements).length > 0) {
        oneIncomingActive = true;
      }

      if(setIntersection(greenHash[orJ.id], tokenElements).length > 0) {
        activationIntersection = false;
      }

      if(oneIncomingActive) {
        if(activationIntersection) {
          activateElement(orJ, canvas);
        }
        else {
          waitingElement(orJ, canvas);
        }
      }
    }
  });

  // activate AND joins
  andJoins.forEach(function(andJ) {
    if(andJ.status == SELECTABLE_STATUS) {
      // do nothing because the gate is subject to a selection process
    }
    else {
      var equalSets = true;
      var oneIncomingActive = false;

      var inter = setIntersection(andJ.incoming, tokenElements);

      // TODO - not really happy with just the length test here
      if(inter.length != andJ.incoming.length) {
        equalSets = false;
      }

      if(inter.length > 0) {
        oneIncomingActive = true;
      }

      if(oneIncomingActive) {
        if(equalSets) {
          activateElement(andJ, canvas);
        }
        else {
          waitingElement(andJ, canvas);
        }
      }
    }
  });

  // activate XOR joins
  xorJoins.forEach(function(xorJ) {
    var oneIncomingActive = false;

    if(setIntersection(xorJ.incoming, tokenElements).length > 0) {
      activateElement(xorJ, canvas);
    }
  });
}

function updateTokens(element, elementRegistry) {
  // Update elements with tokens
  tokenElements = tokenElements.filter(function(el) {
    return element.incoming.indexOf(el) < 0;
  });

  element.outgoing.forEach(function(outgoingFlow){
    tokenElements.push(outgoingFlow);
  });
}

function orJoinActiveIncome(orJoin) {
  var oneIncomingActive = false;

  if(setIntersection(orJoin.incoming, tokenElements).length > 0) {
    oneIncomingActive = true;
  }

  return oneIncomingActive;
}

function testORJoinActivation(orJoin) {
  var activationIntersection = true;

  if(setIntersection(greenHash[orJoin.id], tokenElements). length > 0) {
    activationIntersection = false;
  }

  return activationIntersection;
}

function handleGate(element, elementRegistry, canvas, selectionId) {
  if(isXORGateway(element)) {
    // handle XOR Split
    if(successorsLists[element.id].length > 1) {
      setElementAsExecuted(element, canvas);
      updateTokens(element, elementRegistry);

      // Test if the outgoing connections contain a condition which can be
      // evaluated to true
      var nextElementFound = null;

      element.outgoing.forEach(function(entry) {
        if(entry.businessObject.conditionExpression != null && !nextElementFound) {
          if(eval('businessData.'
                        + entry.businessObject.conditionExpression.body)) {
            nextElementFound = entry;
          }
        }
      });

      var nextElements = [];

      // Get all elements which are successors of the current gateway
      successorsLists[element.id].forEach(function(entry) {
        nextElements.push(getElementByName(elementRegistry, entry));
      });

      // The next element can be determined from the flow condition
      if(nextElementFound) {
        nextElements.forEach(function(entry) {
          if(entry.incoming.indexOf(nextElementFound) >= 0) {
            setElementAsExecuted(entry, canvas);
            activeElements = activeElements.concat(successorsLists[entry.id]);
            activeElements = activeElements.filter(function(item, pos) {
              return activeElements.indexOf(item) == pos;
            });
            updateTokens(entry, elementRegistry);
          }
          else {
            removeMarkers(entry, canvas);
          }
        });
      }
      // The next element must be determined manually
      else {
        // mark all successors as selectable
        nextElements.forEach(function(entryElement) {
          selectableElement(entryElement, canvas);
          entryElement.selectableId = selectionId;
          selectableElements.push(entryElement);
        });
      }
    }
    // handle XOR Join
    else {
      setElementAsExecuted(element, canvas);
      updateTokens(element, elementRegistry);

      var nextElement = getElementByName(elementRegistry,
                                          successorsLists[element.id][0]);

      if(isGateway(nextElement)) {
        activeElements.push(nextElement.id);
      }
      else {
        setElementAsExecuted(nextElement, canvas);
        updateTokens(nextElement, elementRegistry);
        activeElements = activeElements.concat(successorsLists[nextElement.id]);
        activeElements = activeElements.filter(function(item, pos) {
          return activeElements.indexOf(item) == pos;
        });
      }
    }
  }
  else if(isANDGateway(element)) {
    // handle AND Split
    if(successorsLists[element.id].length > 1) {
      setElementAsExecuted(element, canvas);
      updateTokens(element, elementRegistry);

      var nextElements = [];

      // Get all elements which are successors of the current gateway
      successorsLists[element.id].forEach(function(entry) {
        nextElements.push(getElementByName(elementRegistry, entry));
      });

      // mark all successors as selectable
      nextElements.forEach(function(entryElement) {
        selectableElement(entryElement, canvas);
        entryElement.selectableId = selectionId + entryElement.id;
        selectableElements.push(entryElement);
      });
    }
    // handle AND Join
    else {

      // all the incoming elements have been executed
      if(element.status == EXECUTABLE_STATUS) {
        setElementAsExecuted(element, canvas);
        updateTokens(element, elementRegistry);

        var nextElement = getElementByName(elementRegistry,
                                            successorsLists[element.id][0]);

        if(isGateway(nextElement)) {
          activeElements.push(nextElement.id);
        }
        else {
          setElementAsExecuted(nextElement, canvas);
          updateTokens(nextElement, elementRegistry);
          activeElements = activeElements.concat(successorsLists[nextElement.id]);
          activeElements = activeElements.filter(function(item, pos) {
            return activeElements.indexOf(item) == pos;
          });
        }
      }
    }
  }
  else if(isEventBasedGateway(element)) {

    // handle Event Base Split
    if(successorsLists[element.id].length > 1) {
      setElementAsExecuted(element, canvas);
      updateTokens(element, elementRegistry);

      var nextElements = [];

      // Get all elements which are successors of the current gateway
      successorsLists[element.id].forEach(function(entry) {
        nextElements.push(getElementByName(elementRegistry, entry));
      });

      // mark all successors as selectable
      nextElements.forEach(function(entryElement) {
        selectableElement(entryElement, canvas);
        entryElement.selectableId = selectionId;
        selectableElements.push(entryElement);
      });
    }
  }
  else if(isORGateway(element)) {
    // handle OR Split
    if(successorsLists[element.id].length > 1) {
      setElementAsExecuted(element, canvas);
      updateTokens(element, elementRegistry);

      var nextElements = [];

      // Get all elements which are successors of the current gateway
      successorsLists[element.id].forEach(function(entry) {
        nextElements.push(getElementByName(elementRegistry, entry));
      });

      // mark all successors as selectable
      nextElements.forEach(function(entryElement) {
        selectableElement(entryElement, canvas);
        entryElement.selectableId = selectionId + entryElement.id;
        entryElement.deactivable = true;
        selectableElements.push(entryElement);
      });
    }
    // handle OR Join
    else {
      // if the OR join has at least one active incoming
      if(orJoinActiveIncome(element)) {
        // if the gate is ready to be executed
        if(testORJoinActivation(element)) {
          setElementAsExecuted(element, canvas);
          updateTokens(element, elementRegistry);

          var nextElement = getElementByName(elementRegistry,
                                              successorsLists[element.id][0]);

          if(isGateway(nextElement)) {
            activeElements.push(nextElement.id);
          }
          else {
            setElementAsExecuted(nextElement, canvas);
            updateTokens(nextElement, elementRegistry);
            activeElements = activeElements.concat(successorsLists[nextElement.id]);
            activeElements = activeElements.filter(function(item, pos) {
              return activeElements.indexOf(item) == pos;
            });
          }
        }
        else {
          waitingElement(element, canvas);
        }
      }
    }
  }
}

function removeElementsForSelection(selectionId, canvas) {
  selectableElements.forEach(function(element) {
    if(element.selectableId == selectionId) {
      selectableElements.splice(selectableElements.indexOf(element), 1);
      delete element.selectableId;
      element.status = '';
      removeMarkers(element, canvas);
      // remove the potential tokens from its incoming edges
      element.incoming.forEach(function(con) {
        var index = tokenElements.indexOf(con);
        if(index >= 0) {
          tokenElements.splice(index, 1);
        }
      });
    }
  });
}

function setElementAsExecuted(element, canvas) {
  highlightElement(element, canvas);

  // Open a form whenever we reach a task
  if(isTask(element)) {
    // if it is script task evaluate the script
    if(isScriptTask(element) && element.businessObject.script != null) {
      eval("businessData." + element.businessObject.script.body);
      updateConditionVariables();
    }
    // if is a different type of task open the window
    else {
      newTaskWindow(element);
    }
  }
  else if(isStartEvent(element)) {
    newTaskWindow(element);
  }
}

function deselectElement(element, canvas, elementRegistry) {
  removeElementsForSelection(element.selectableId, canvas);
  delete element.selectableId;

  // Compute the hashtables for each OR join
  updateORHashes(elementRegistry);

  activateNextElements(elementRegistry, canvas);
}

function selectElement(element, canvas, elementRegistry) {
  selectableElements.splice(selectableElements.indexOf(element), 1);
  removeElementsForSelection(element.selectableId, canvas);
  delete element.selectableId;

  handleElement(element, canvas, elementRegistry, false);

  // Compute the hashtables for each OR join
  updateORHashes(elementRegistry);

  activateNextElements(elementRegistry, canvas);
}

// TODO - Check if the hardcoded code can be removed
function initializaBussinesData(elementRegistry, moodle) {
  businessData = {};
  formProperties = [];

  //businessData.testCondition = false;

  //---------Hardcoded----------------------------

  // Hardcoded for loop2
  var xorJoin = getElementByName(elementRegistry, "ExclusiveGateway_1sxsj1d");

  if(xorJoin != null) {
    elementOverlays["ExclusiveGateway_1sxsj1d"] =
                                      '<div>Test Condition true?</div>';
  }

  var connection1 = getElementByName(elementRegistry, "SequenceFlow_16gpbhn");

  if(connection1 != null) {
    elementConditions["SequenceFlow_16gpbhn"] =
                                      moodle.create('bpmn:FormalExpression', {
      body: 'businessData.testCondition == true'
    });
  }

  var connection2 = getElementByName(elementRegistry, "SequenceFlow_0zf247m");

  if(connection1 != null) {
    elementConditions["SequenceFlow_0zf247m"] =
                                      moodle.create('bpmn:FormalExpression', {
      body: 'businessData.testCondition == false'
    });
  }
  //----------------------------------------------
}

function initializeSimulationData(elementRegistry, canvas) {
  simulationBegan = false;

  stepButton = document.getElementById("stepButton");
  beginButton = document.getElementById("beginButton");

  stepButton.disabled = true;
  // at each step execute the required steps
  stepButton.addEventListener("click", function(){

    var highlightElements = activeElements.slice();
    activeElements = [];
    highlightElements.forEach(function(entry) {
      var element = getElementByName(elementRegistry, entry);
      handleElement(element, canvas, elementRegistry, true);
    });

    // Compute the hashtables for each OR join
    updateORHashes(elementRegistry);

    activateNextElements(elementRegistry, canvas);

  });

  beginButton.addEventListener("click", function(){
    stepButton.disabled = false;
    beginButton.disabled = true;

    simulationBegan = true;

    // create the new model inside the editor with the properties specified in
    // the previous pahse of the simulation
    initilizeTaskWindowEditor();

    // get a list of all the active elements which can be executed at the start
    // of the process - only the start events
    activeElements = getInitialActiveElements(elementRegistry);

    // set executable elements in the begining
    activeElements.forEach(function(entry) {
      activateElement(getElementByName(elementRegistry, entry), canvas);
    });
  });
}
//------------------------------------------------------------------------------
function showDiagram(diagramXML) {

  viewer.importXML(diagramXML, function() {
    var eventBus = viewer.get('eventBus');
    var overlays = viewer.get('overlays'),
        canvas = viewer.get('canvas'),
        elementRegistry = viewer.get('elementRegistry'),
        moodle = viewer.get('moddle');

    initializeJsonEditorData();

    console.log(window.Dropzone);

    // construct the successors lists for each element when the page is loaded
    successorsLists = {};
    // construct the successors lists for each element when the page is loaded
    predecessorsLists = {};
    // contains the elements which are ready to be executed at each point in
    // time
    activeElements = [];

    // list containing the elements which require a decision
    selectableElements = [];

    // initialize bussinnes data
    initializaBussinesData(elementRegistry, moodle);

    // initilize the data related to the start of the simulation
    initializeSimulationData(elementRegistry, canvas);

    // construct the successorsLists and predecessorsLists
    elementRegistry.forEach(function(element) {

      // verify the element is an event, task, or gateway
      if(isExecutableElement(element)) {

        // construct successors lists
        var connectedIds = [];

        // search all the elements connected to a specific connection
        element.outgoing.forEach(function(connection) {

          // make sure the element is a connection element
          if(isConnection(connection)) {

            elementRegistry.forEach(function(element) {
              if(element.incoming.indexOf(connection) >= 0) {

                // add the id to the arrays of ids connected to the current
                // element
                connectedIds.push(element.id);
              }
            });
            connection.incoming.push(element);
          }
        });

        successorsLists[element.id] = connectedIds;

        // construct predecessors lists
        connectedIds = [];

        element.incoming.forEach(function(connection) {
          if(isConnection(connection)) {

            elementRegistry.forEach(function(element) {
              if(element.outgoing.indexOf(connection) >= 0) {
                connectedIds.push(element.id);
              }
            });
            connection.outgoing.push(element);
          }
        });

        predecessorsLists[element.id] = connectedIds;
      }

      // Identify all the OR joins in the diagram
      if(isORJoinGateway(element)) {
        orJoins.push(element);
      }
      else if(isANDJoinGateway(element)) {
        andJoins.push(element);
      }
      else if(isXORJoinGateway(element)) {
        xorJoins.push(element);
      }
    });

    eventBus.on('element.click', function (e){
      if(simulationBegan) {
        console.log(event, 'on', e.element);

        // if the element may be deactivated - present the activation window first
        if(e.element.deactivable && e.element.status == SELECTABLE_STATUS) {
          //activationWindow(e.element, canvas, elementRegistry);
          newActivationWindow(e.element, canvas, elementRegistry);
        }
        // highlight the element only if it is marked as executable
        else if(e.element.status == SELECTABLE_STATUS) {
          selectElement(e.element, canvas, elementRegistry);
        }
      }
      else {
        if(isScriptTask(e.element)) {
          newScriptWindow(e.element, moodle);
        }
        else if(isConnection(e.element)) {
          newConditionWindow(e.element, moodle);
        }
        else if(isGateway(e.element)) {
          newGateOverlayWindow(e.element, overlays, moodle);
        }
        else if(isStartEvent(e.element)) {
          newStartEventWindow(e.element);
        }
      }
    });
  });
}

// load + show diagram
//$.get('resources/pizza-collaboration.bpmn', showDiagram);
$.get('resources/orSplit.bpmn', showDiagram);
